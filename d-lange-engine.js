/**
 * 喜欢fp的可以加群426566496
 * @fileOverview
 * @name d-lange-engine.js
 * @author diqye
 * @license
 */
(function(exports, R) {
    var head = R.head,
        tail = R.tail;
    var conf = [
        [isBool, boolfn],
        [isString, stringfn],
        [isNumber, numberfn],
        [isArray, arrayfn],
        [R.equals("null"), R.always(null)],
        [R.T, otherfn]
    ];
    var tokenConf = {
        arrStart: "'(",
        arrEnd: ")"
    };

    function otherfn(selector) {
        return {
            I: "other",
            token: selector
        };
    }

    function isBool(str) {
        return str == "true" || str == "false";
    }

    function boolfn(str) {
        return str === "true";
    }

    function isNumber(str) {
        return isNaN(Number(str)) == false;
    }

    function numberfn(str) {
        return Number(str);
    }

    function isString(str) {
        return head(str) == "'";
    }

    function stringfn(str) {
        return tail(str);
    }

    function isArray(str) {
        return R.take(3, str) == "[] ";
    }

    function arrayfn(str) {
        return parserArg(R.drop(3, str), tokenConf);
    }

    function parser(str) {
        var token = tokenfn(ltrim(str), " ", "", 0, tokenConf);
        if (R.length(token) < 2) return {
            fn: head(token),
            args: []
        };
        return {
            fn: head(token),
            args: parserArg(R.last(token), tokenConf)
        };
    }

    function parserArg(str, config) {
        if (isSpaces(str)) return [];
        var token = tokenfn(ltrim(str), " ", "", 0, config);
        if (R.length(token) == 1) return [R.cond(conf)(head(token))];
        return R.concat([R.cond(conf)(head(token))], parserArg(R.last(token), config));
    }

    function ltrim(str) {
        if (head(str) == " ") return ltrim(tail(str));
        return str;
    }

    function isSpaces(str) {
        if (str == "") return true;
        if (head(str) !== " ") return false;
        return isSpaces(tail(str));
    }

    function tokenfn(str, endflag, t, arrnum, config) {
        if (str == null || str == "") {
            if (arrnum != 0) throw "dlang-error:数组括号不匹配";
            return [t];
        }
        if (head(str) == endflag) {
            if (endflag == config.arrEnd) {
                if (arrnum == 1) return [t, tail(str)];
                return tokenfn(tail(str), endflag, t + head(str), arrnum - 1, config);
            }
            return [t, tail(str)];
        }
        if (R.pipe(R.take(config.arrStart.length), R.equals(config.arrStart))(str)) {
            if (arrnum == 0) {
                if (!R.isEmpty(t)) return [t, str];
                return tokenfn(R.drop(2, str), ")", "[] ", arrnum + 1, config);
            }
            return tokenfn(tail(str), endflag, t + head(str), arrnum + 1, config);
        }
        return tokenfn(tail(str), endflag, t + head(str), arrnum, config);
    }

    function parserWithPipe(str) {
        if (!str) return null;
        var flag = "$       7       $";
        return R.map(R.pipe(R.split(flag), R.join("'|>"), parser), str.split("'|>").join(flag).split("|>"));
    }
    exports.dlang = {
        parser: parser,
        parserWithPipe: parserWithPipe
    };
}(this, R));
