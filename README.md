#dpipe
[在线演示](http://sandbox.runjs.cn/show/8lousjkg)
- 包含两部分
- 一部分是语法解析
- 一部分是将语法运用在html中

解析语法部分可以直接看演示

#html
```html
<input input-d type="text"
                       class="form-control dinput"
                       value="函数名字 '字符串参数"
                       d-keyup="val this|>parser|>tojson|>htmlel [output]"
                       placeholder="输入语法">
```
```d-keyup``` 等同于调用传进来scope函数
```javascript
htmlel(el.find("[output]"),tojson(parser(val(el.find(this)))))
```

这个是面向函数的思路 函数和函数之间可以组合 函数可以操作dom也可以操作数据

- 例子(jq模板)


```html
{{if x.selected=true}}
<input type="checkbox" checked="checked" d-click="set 'x false|>render"/>
{{else}}
<input type="checkbox" d-click="set 'x true|>render" />
{{/if}}
<script>
var data={x:true}
function render(data){
   return xxx.html(templatefn(data));
}
function set(x,val){
 data[x]=val;
 return data;
}
dpipe.dpipe($("xx"),{
 set:set,
 render:render   
})
</script>
```